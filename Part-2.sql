CREATE DATABASE SkiersLogs;

USE SkiersLogs;

CREATE TABLE skiers(skUserName varchar(10) NOT NULL, skFirstName varchar(25) NOT NULL, skLastName varchar(60) NOT NULL, skyob YEAR(4) NOT NULL,
  PRIMARY KEY(skUserName));

CREATE TABLE clubs(cID varchar(10) NOT NULL, cName varchar(60) NOT NULL, cCity varchar(60) NOT NULL, cCounty varchar(60) NOT NULL,
  PRIMARY KEY(cID));

CREATE TABLE seasons(sFallYear YEAR(4) NOT NULL,
	PRIMARY KEY(sFallYear));

CREATE TABLE skis(skUserName varchar(10) NOT NULL,cID varchar(10) NOT NULL, sFallYear YEAR(4) NOT NULL, skiDate date NOT NULL, skiArea varchar(60) NOT NULL, skiDistance int(3) NOT NULL,
  PRIMARY KEY(skUserName, cID, sFallYear),
  FOREIGN KEY(skUserName) REFERENCES skiers(skUserName),
  FOREIGN KEY(cID) REFERENCES clubs(cID),
  FOREIGN KEY(sFallYear) REFERENCES seasons(sFallYear)
  );
  
CREATE TABLE totdist(skUserName varchar(10) NOT NULL, sFallYear YEAR(4) NOT NULL, totdist int(5) NOT NULL,
	PRIMARY KEY(skUserName, sFallYear),
	FOREIGN KEY(skUserName) REFERENCES skiers(skUserName),
	FOREIGN KEY(sFallYear) REFERENCES seasons(sFallYear)
	);