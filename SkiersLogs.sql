-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 05. Nov, 2017 23:41 PM
-- Server-versjon: 5.7.20-0ubuntu0.17.04.1
-- PHP Version: 7.0.22-0ubuntu0.17.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `SkiersLogs`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `clubs`
--

CREATE TABLE `clubs` (
  `cID` varchar(10) NOT NULL,
  `cName` varchar(60) NOT NULL,
  `cCity` varchar(60) NOT NULL,
  `cCounty` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `clubs`
--

INSERT INTO `clubs` (`cID`, `cName`, `cCity`, `cCounty`) VALUES
('asker-ski', 'Asker skiklubb', 'Asker', 'Akershus'),
('lhmr-ski', 'Lillehammer Skiklub', 'Lillehammer', 'Oppland'),
('skiklubben', 'Trondhjems skiklub', 'Trondheim', 'Sør-Trøndelag'),
('vindil', 'Vind Idrettslag', 'Gjøvik', 'Oppland');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `seasons`
--

CREATE TABLE `seasons` (
  `sFallYear` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `seasons`
--

INSERT INTO `seasons` (`sFallYear`) VALUES
(2015),
(2016);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skiers`
--

CREATE TABLE `skiers` (
  `skUserName` varchar(10) NOT NULL,
  `skFirstName` varchar(25) NOT NULL,
  `skLastName` varchar(60) NOT NULL,
  `skyob` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `skiers`
--

INSERT INTO `skiers` (`skUserName`, `skFirstName`, `skLastName`, `skyob`) VALUES
('ande_andr', 'Anders', 'Andresen', 2004),
('ande_rønn', 'Anders', 'Rønning', 2001),
('andr_stee', 'Andreas', 'Steen', 2001),
('anna_næss', 'Anna', 'Næss', 2005),
('arne_anto', 'Arne', 'Antonsen', 2005),
('arne_inge', 'Arne', 'Ingebrigtsen', 2005),
('astr_amun', 'Astrid', 'Amundsen', 2001),
('astr_sven', 'Astrid', 'Svendsen', 2008),
('bent_håla', 'Bente', 'Håland', 2009),
('bent_svee', 'Bente', 'Sveen', 2003),
('beri_hans', 'Berit', 'Hanssen', 2003),
('bjør_aase', 'Bjørn', 'Aasen', 2006),
('bjør_ali', 'Bjørn', 'Ali', 2008),
('bjør_rønn', 'Bjørg', 'Rønningen', 2009),
('bjør_sand', 'Bjørn', 'Sandvik', 1997),
('bror_kals', 'Bror', 'Kalstad', 2006);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skis`
--

CREATE TABLE `skis` (
  `skUserName` varchar(10) NOT NULL,
  `cID` varchar(10) NOT NULL,
  `sFallYear` year(4) NOT NULL,
  `skiDate` date NOT NULL,
  `skiArea` varchar(60) NOT NULL,
  `skiDistance` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `skis`
--

INSERT INTO `skis` (`skUserName`, `cID`, `sFallYear`, `skiDate`, `skiArea`, `skiDistance`) VALUES
('andr_stee', 'asker-ski', 2015, '2015-11-16', 'Dikemark', 25);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `totdist`
--

CREATE TABLE `totdist` (
  `skUserName` varchar(10) NOT NULL,
  `sFallYear` year(4) NOT NULL,
  `totdist` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clubs`
--
ALTER TABLE `clubs`
  ADD PRIMARY KEY (`cID`);

--
-- Indexes for table `seasons`
--
ALTER TABLE `seasons`
  ADD PRIMARY KEY (`sFallYear`);

--
-- Indexes for table `skiers`
--
ALTER TABLE `skiers`
  ADD PRIMARY KEY (`skUserName`);

--
-- Indexes for table `skis`
--
ALTER TABLE `skis`
  ADD PRIMARY KEY (`skUserName`,`cID`,`sFallYear`),
  ADD KEY `cID` (`cID`),
  ADD KEY `sFallYear` (`sFallYear`);

--
-- Indexes for table `totdist`
--
ALTER TABLE `totdist`
  ADD PRIMARY KEY (`skUserName`,`sFallYear`),
  ADD KEY `sFallYear` (`sFallYear`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `skis`
--
ALTER TABLE `skis`
  ADD CONSTRAINT `skis_ibfk_1` FOREIGN KEY (`skUserName`) REFERENCES `skiers` (`skUserName`),
  ADD CONSTRAINT `skis_ibfk_2` FOREIGN KEY (`cID`) REFERENCES `clubs` (`cID`),
  ADD CONSTRAINT `skis_ibfk_3` FOREIGN KEY (`sFallYear`) REFERENCES `seasons` (`sFallYear`);

--
-- Begrensninger for tabell `totdist`
--
ALTER TABLE `totdist`
  ADD CONSTRAINT `totdist_ibfk_1` FOREIGN KEY (`skUserName`) REFERENCES `skiers` (`skUserName`),
  ADD CONSTRAINT `totdist_ibfk_2` FOREIGN KEY (`sFallYear`) REFERENCES `seasons` (`sFallYear`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
