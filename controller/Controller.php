<?php
include_once("connection.php");
include_once("model/Model.php");


/** The Controller is responsible for handling user requests, for exchanging data with the Model,
 * and for passing user response data to the various Views.
 * @author Rune Hjelsvold
 * @see model/Model.php The Model class holding book data.
 * @see view/viewbook.php The View class displaying information about one book.
 * @see view/booklist.php The View class displaying information about all books.
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class Controller {


	public function __construct()
    {
		session_start();
        $this->model = new Model();
				$this->doc = new DOMDocument;
				$this->doc->load('assignment-5/SkiersLogs.xml');
				$this->xpath = new DOMXPath($this->doc);
    }

/** The one function running the controller code.
 */
	public function invoke()
	{
			//  Run importSkiers
			$query = '//SkierLogs/Skiers/Skier';
			$elements = $this->xpath->query($query);
			$this->model->importSkiers($elements);

			//  Run importClubs
			$query = "//Club";
			$elements = $this->xpath->query($query);
			$this->model->importClubs($elements);

			//  Run importSeasons
			$query = "//Season";
			$elements = $this->xpath->query($query);
			$this->model->importSeasons($elements);

			$this->model->importSkis($elements);
	}
}

?>
