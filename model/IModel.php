<?php

Interface IModel {

public function getNode($el, $tag);

public function importSkiers($skiers);

public function importClubs($clubs);

public function importSeasons($seasons);

public function importSkis($skis);

public function importTotdist($distance);
}
?>
