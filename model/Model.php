<?php
include_once("IModel.php");


class Model implements IModel{

  protected $db = null;

  public function __construct($db = null){
    if($db){
      $this->db = $db;
    }
    else{
      try{
        $this->db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8',
                           DB_USER, DB_PWD,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
         }
         catch(PDOException $e){
           echo $e->getMessage();
         }
       }
  }

  public function getNode($el, $tag){
    return $el->getElementsByTagName($tag)->item(0)->textContent;
  }

  public function importSkiers($skiers){
    if (!is_null($skiers)){
    //Preparing PDO INSERT Statment
      foreach ($skiers as $skier) {
        $tskier = $skier->getAttribute('userName');
        $fname = $skier->getElementsByTagName('FirstName')->item(0)->textContent;
        $lname = $skier->getElementsByTagName('LastName')->item(0)->textContent;
        $yob = $skier->getElementsByTagName('YearOfBirth')->item(0)->textContent;
        $stmt = $this->db->prepare("INSERT INTO skiers(skUserName, skFirstName, skLastName, skyob) VALUES(?,?,?,?)");

        $stmt->bindValue(1, $tskier , PDO::PARAM_STR);
        $stmt->bindValue(2, $fname , PDO::PARAM_STR);
        $stmt->bindValue(3, $lname, PDO::PARAM_STR);
        $stmt->bindValue(4, $yob, PDO::PARAM_INT);
        $stmt->execute();
      }
    }
  }

  public function importClubs($clubs){
    if (!is_null($clubs)){
      foreach ($clubs as $club) {
          $stmt = $this->db->prepare("INSERT INTO clubs(cID, cName, cCity, cCounty) VALUES(?,?,?,?)");

          $tclub = $club->getAttribute('id');
          $name = $club->getElementsByTagName('Name')->item(0)->textContent;
          $city = $club->getElementsByTagName('City')->item(0)->textContent;
          $county = $club->getElementsByTagName('County')->item(0)->textContent;

          $stmt->bindValue(1, $tclub, PDO::PARAM_STR);
          $stmt->bindValue(2, $name, PDO::PARAM_STR);
          $stmt->bindValue(3, $city , PDO::PARAM_STR);
          $stmt->bindValue(4, $county , PDO::PARAM_STR);
          $stmt->execute();
       }
    }
  }

  public function importSeasons($seasons){
    if(!is_null($seasons)){
      foreach ($seasons as $season) {
        $stmt = $this->db->prepare("INSERT INTO seasons(sFallYear) VALUES (?)");
        $stmt->bindValue(1, $season->getAttribute('fallYear'), PDO::PARAM_INT);
        $stmt->execute();
      }
    }
  }

  public function importSkis($skis){
    if(!is_null($skis)){
      foreach ($skis as $eachseason) {
        $stmt = $this->db->prepare("INSERT INTO skis(skUserName, cID, sFallYear, skiDate, skiArea, skiDistance) VALUES(?,?,?,?,?,?)");
        $season = $eachseason->getAttribute('fallYear');
        $stmt->bindValue(3, $season, PDO::PARAM_INT);

        foreach ($eachseason->getElementsByTagName('Skiers') as $eachclub){
          $club = $eachclub->getAttribute('clubId');
          $stmt->bindValue(2, $club, PDO::PARAM_STR);

          foreach ($eachclub->getElementsByTagName('Skier') as $eachskier) {
            $skier = $eachskier->getAttribute('userName');
            $stmt->bindValue(1, $skier, PDO::PARAM_STR);

              foreach ($eachskier->getElementsByTagName('Entry') as $eachentry) {
                $date = $eachentry->getElementsByTagName('Date')->item(0)->textContent;
                $area = $eachentry->getElementsByTagName('Area')->item(0)->textContent;
                $distance = $eachentry->getElementsByTagName('Distance')->item(0)->textContent;
                $stmt->bindValue(4, $date, PDO::PARAM_STR);
                $stmt->bindValue(5, $area, PDO::PARAM_STR);
                $stmt->bindValue(6, $distance, PDO::PARAM_INT);
                $stmt->execute();
              }
          }
        }

      }
    }
  }

  public function importTotdist($distance){
    if(!is_null($distance)){
      $stmt = $this->db->prepare("INSERT INTO totdist(skUserName, sFallYear, totdist) VALUES(?,?,?)");

    }
  }

}
?>
